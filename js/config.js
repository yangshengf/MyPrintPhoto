/**
 * 云服务配置文件
 */

var CONFIG={};

CONFIG.appName="我的冲印";//APP 名称（必填）
CONFIG.appRootUrl="http://117.78.52.248:8080/PhotoCloudServer/";//ajax 请求服务根目录地址（必填）
CONFIG.appRootBackUrl="http://117.78.52.248:8080/PhotoCloudServer/";//ajax 请求服务根目录备份地址，用于请求失败重连（选填）
CONFIG.DEBUG=true;//是否是debug模式（必填）
CONFIG.UPLOADER_SERVER="http://117.78.52.248:8080/PhotoCloudServer/Upload";//照片上传php服务端