var CACHE={};

///记录登录
CACHE.Login=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('Login');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('Login');
        return;
    }
    plus.storage.setItem('Login', arguments[0]);
}

///用户名
CACHE.UserName=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('UserName');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('UserName');
        return;
    }
    plus.storage.setItem('UserName', arguments[0]);
}